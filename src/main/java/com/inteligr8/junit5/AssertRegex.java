package com.inteligr8.junit5;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.jupiter.api.Assertions;

/**
 * This class implements JUnit4 assertion methods focused on regular expression matching.
 * 
 * @author brian@inteligr8.com
 */
public class AssertRegex {
	
	/**
	 * This method evaluates the format of the specified value using the
	 * specified regular expression.
	 * 
	 * @param expectedRegex A regular expression
	 * @param actual A text value to evaluate
	 * @see AssertRegex#assertNotMatches(String, String)
	 */
	public static void assertMatches(String expectedRegex, String actual) {
		assertMatches(expectedRegex, actual, null);
	}
	
	/**
	 * This method evaluates the format of the specified value using the
	 * specified regular expression.
	 * 
	 * @param message A message to record on failure
	 * @param expectedRegex A regular expression
	 * @param actual A text value to evaluate
	 * @see AssertRegex#assertNotMatches(String, String, String)
	 */
	public static void assertMatches(String expectedRegex, String actual, String message) {
		Pattern pattern = Pattern.compile(expectedRegex);
		assertMatches(pattern, actual, message);
	}
	
	/**
	 * This method evaluates the format of the specified value using the
	 * specified regular expression.
	 * 
	 * @param expected A regular expression
	 * @param actual A text value to evaluate
	 * @see AssertRegex#assertNotMatches(Pattern, String)
	 */
	public static void assertMatches(Pattern expected, String actual) {
		assertMatches(expected, actual, null);
	}
	
	/**
	 * This method evaluates the format of the specified value using the
	 * specified regular expression.
	 * 
	 * @param message A message to record on failure
	 * @param expected A regular expression
	 * @param actual A text value to evaluate
	 * @see AssertRegex#assertNotMatches(Pattern, String, String)
	 */
	public static void assertMatches(Pattern expected, String actual, String message) {
		assertMatches(false, expected, actual, message);
	}
	
	/**
	 * This method negates the evaluation of the format of the specified value
	 * using the specified regular expression.
	 * 
	 * @param expectedRegex A regular expression
	 * @param actual A text value to evaluate
	 * @see AssertRegex#assertMatches(String, String)
	 */
	public static void assertNotMatches(String expectedRegex, String actual) {
		assertNotMatches(expectedRegex, actual, null);
	}
	
	/**
	 * This method negates the evaluation of the format of the specified value
	 * using the specified regular expression.
	 * 
	 * @param message A message to record on failure
	 * @param expectedRegex A regular expression
	 * @param actual A text value to evaluate
	 * @see AssertRegex#assertMatches(String, String, String)
	 */
	public static void assertNotMatches(String expectedRegex, String actual, String message) {
		Pattern pattern = Pattern.compile(expectedRegex);
		assertNotMatches(pattern, actual, message);
	}
	
	/**
	 * This method negates the evaluation of the format of the specified value
	 * using the specified regular expression.
	 * 
	 * @param expected A regular expression
	 * @param actual A text value to evaluate
	 * @see AssertRegex#assertMatches(Pattern, String)
	 */
	public static void assertNotMatches(Pattern expected, String actual) {
		assertNotMatches(expected, actual, null);
	}
	
	/**
	 * This method negates the evaluation of the format of the specified value
	 * using the specified regular expression.
	 * 
	 * @param message A message to record on failure
	 * @param expected A regular expression
	 * @param actual A text value to evaluate
	 * @see AssertRegex#assertMatches(Pattern, String, String)
	 */
	public static void assertNotMatches(Pattern expected, String actual, String message) {
		assertMatches(true, expected, actual, message);
	}
	
	private static void assertMatches(boolean negate, Pattern expected, String actual, String message) {
		Matcher matcher = expected.matcher(actual);
		if (matcher.matches() == negate) {
			message = message == null ? "" : (message + "; ");
			message += "expression of expected string: <" + expected.toString() + "> but actual string: <" + actual + ">";
			Assertions.fail(message);
		}
	}
	
	
	
	/**
	 * This method counts the matches of the specified value against the
	 * specified regular expression.
	 * 
	 * @param expectedRegex A regular expression
	 * @param expectedCount The count of an expected regex match
	 * @param actual A text value to evaluate
	 * @see AssertRegex#assertMatches(String, String)
	 */
	public static void assertFind(String expectedRegex, int expectedCount, String actual) {
		assertFind(expectedRegex, expectedCount, actual, null);
	}
	
	/**
	 * This method counts the matches of the specified value against the
	 * specified regular expression.
	 * 
	 * @param message A message to record on failure
	 * @param expectedRegex A regular expression
	 * @param expectedCount The count of an expected regex match
	 * @param actual A text value to evaluate
	 * @see AssertRegex#assertMatches(String, String, String)
	 */
	public static void assertFind(String expectedRegex, int expectedCount, String actual, String message) {
		Pattern pattern = Pattern.compile(expectedRegex);
		assertFind(pattern, expectedCount, actual, message);
	}
	
	/**
	 * This method counts the matches of the specified value against the
	 * specified regular expression.
	 * 
	 * @param expected A regular expression
	 * @param expectedCount The count of an expected regex match
	 * @param actual A text value to evaluate
	 * @see AssertRegex#assertMatches(Pattern, String)
	 */
	public static void assertFind(Pattern expected, int expectedCount, String actual) {
		assertFind(expected, expectedCount, actual, null);
	}
	
	/**
	 * This method counts the matches of the specified value against the
	 * specified regular expression.
	 * 
	 * @param message A message to record on failure
	 * @param expected A regular expression
	 * @param expectedCount The count of an expected regex match
	 * @param actual A text value to evaluate
	 * @see AssertRegex#assertMatches(Pattern, String)
	 */
	public static void assertFind(Pattern expected, int expectedCount, String actual, String message) {
		Matcher matcher = expected.matcher(actual);
		int count = 0;
		for (count = 0; matcher.find(); count++)
			;
		if (expectedCount < 0) Assertions.assertTrue(-expectedCount <= count, message);
		else Assertions.assertEquals(expectedCount, count, message);
	}

}
