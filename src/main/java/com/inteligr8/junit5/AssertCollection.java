package com.inteligr8.junit5;

import java.util.Collection;

import org.junit.jupiter.api.Assertions;

/**
 * This class implements JUnit4 assertion methods focused on collections.
 * 
 * @author brian@inteligr8.com
 */
public class AssertCollection {
	
	/**
	 * This method evaluates the size of a collection.
	 * 
	 * @param expectedCount The expected size of the collection
	 * @param actualCollection The collection to evaluate
	 */
	public static void assertSize(int expectedCount, Collection<?> actualCollection) {
		assertSize(expectedCount, actualCollection, null);
	}
	
	/**
	 * This method evaluates the size of a collection.
	 * 
	 * @param message A message to record on failure
	 * @param expectedCount The expected size of the collection
	 * @param actualCollection The collection to evaluate
	 */
	public static void assertSize(int expectedCount, Collection<?> actualCollection, String message) {
		if (expectedCount == 0) Assertions.assertTrue(actualCollection == null || actualCollection.isEmpty(), message);
		else Assertions.assertTrue(actualCollection != null && actualCollection.size() == expectedCount, message);
	}
	
	/**
	 * This method evaluates the contents of a collection
	 * 
	 * @param expectedElement A expected element in the collection
	 * @param actualCollection A collection to search/inspect
	 */
	public static <T> void assertContains(T expectedElement, Collection<T> actualCollection) {
		assertContains(expectedElement, actualCollection, null);
	}
	
	/**
	 * This method evaluates the contents of a collection
	 * 
	 * @param expectedElements A subset of the expected elements in the collection
	 * @param actualCollection A collection to search/inspect
	 */
	public static <T> void assertContains(Collection<T> expectedElements, Collection<T> actualCollection) {
		assertContains(expectedElements, actualCollection, null);
	}
	
	/**
	 * This method evaluates the contents of a collection
	 * 
	 * @param message A message to record on failure
	 * @param expectedElement A expected element in the collection
	 * @param actualCollection A collection to search/inspect
	 */
	public static <T> void assertContains(T expectedElement, Collection<T> actualCollection, String message) {
		Assertions.assertTrue(actualCollection != null && actualCollection.contains(expectedElement), message);
	}
	
	/**
	 * This method evaluates the contents of a collection
	 * 
	 * @param message A message to record on failure
	 * @param expectedElements A subset of the expected elements in the collection
	 * @param actualCollection A collection to search/inspect
	 */
	public static <T> void assertContains(Collection<T> expectedElements, Collection<T> actualCollection, String message) {
		Assertions.assertTrue(actualCollection != null && actualCollection.containsAll(expectedElements), message);
	}

}
